import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import States from './components/states';
import Props from './components/props';

const personalInfo = {
    name: 'John Doe',
    age: '25',
    address: 'QC',
    job: 'developer'
}

function addNumber(num1, num2) {
    return num1 + num2;
}

const addNumbers = (num1, num2) => {
    return num1 + num2;
};

//comment out for states example
//ReactDOM.render(<States />, document.getElementById('root'));
//comment out for props example



//ReactDOM.render(<Props name = 'John' age = '25' address = 'Quezon City' />, document.getElementById('root'));
ReactDOM.render(<Props info = {addNumber} add = {addNumbers} />, document.getElementById('root'));

