import React, {Component} from 'react';

class States extends Component {
    constructor(props) {
        super(props);
        this.state = {   
            value: '',
            output: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmitButton = this.handleSubmitButton.bind(this);

    }

handleInputChange(event) {
    this.setState({value: event.target.value});
    console.log(this.state.value);
}

handleSubmitButton(event) {
    this.setState({output: this.state.value});
    alert("Output: " + this.state.value);
    event.preventDefault();
}    

render () {
      return (
        <div>
            <form onSubmit={this.handleSubmitButton}>
                <p>OUTPUT: {this.state.output}</p>
                <p>INPUT: <input type="text" value={this.state.value} onChange={this.handleInputChange} /> </p>
                <p><input type="submit" value="Submit" /></p>
            </form>
        </div>
      );
    }
  }
  
  export default States;